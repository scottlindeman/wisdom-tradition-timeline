const Snippets = (() => {
  return {
    Desc: String.raw`
<div class="wis-desc">
  <img class="wis-birth-img" src="static/images/VARNICKNAME_birth.png" \>
  <div class="wis-text">
    <span class="wis-blurb">VARTEXT</span>
    <ul class="wis-trads">
      <li class="wis-title"><b>Wisdom Tradition(s):</b></li>
      VARTRADS
    </ul>
  </div>
</div>
`,

  };
})();
