const WT = (() => {

  return {
    main: (TL, tlData) => {
      const tlOptions = {
        font: "lustria-lato",
        hash_bookmark: true,
        scale_factor: 5,
      };

      timeline = new TL.Timeline(
        'timeline-embed',
        tlData,
        tlOptions
      );
    }
  };
})();
