const TimelineData = (() => {

  return (snippets) => {

    const applySnippetSub = (snippet, subs) => {
      return Object.keys(subs).reduce((acc, sk) => {
        let values = subs[sk];
        if (typeof(values) === "object") {
          values = values.reduce((acc, v) => { return acc + `<li>${v}</li>`; }, "");
        }
        return acc.replace(sk, values);
      }, snippet);
    };

    class Wisdom {
      static CH = Symbol("Christianity");
      static BH = Symbol("Buddhism");
      static JD = Symbol("Judaism");
      static IS = Symbol("Islam");
      static TO = Symbol("Taoism");
      static GP = Symbol("Greek Philosophy");
      static CF = Symbol("Confucianism");
      static HU = Symbol("Hinduism");
    }

    class Figure {
      constructor(name, birthYear, deathYear, wikipediaUrl, wisdoms, nickname = "") {
        this.name = name;
        this.nickname = nickname.length > 0 ? nickname : name;
        this.birthYear = birthYear;
        this.deathYear = deathYear;
        this.wisdoms = wisdoms;

        // Eras not used
        this.era = {
          start_date: {
            year: this.birthYear
          },
          end_date: {
            year: this.deathYear
          },
          text: {
            headline: `The Life of ${this.name}`
          }
        };

        this.events = [
          {
            start_date: {year: this.birthYear},
            text: {
              headline: `${this.name} is born`
            },
            media: {
              url: wikipediaUrl
            },
            group: this.nickname
          },
          {
            start_date: {year: this.deathYear},
            text: {
              headline: `${this.nickname} dies`
            },
            group: this.nickname
          }
        ];
      }

      addBirthDescription(birthText) {

        const subbed = applySnippetSub(snippets.Desc, {
          VARNICKNAME: this.nickname.toLowerCase(),
          VARTEXT: birthText,
          VARTRADS: this.wisdoms.map(w => w.description)
        });
        console.log(subbed);
        this.events[0].text.text = subbed;
      }

    }

    const AB = new Figure(
      "Abraham",
      -2150,
      -1975,
      "https://en.wikipedia.org/wiki/Abraham",
      [Wisdom.JD, Wisdom.CH, Wisdom.IS]
    );
    AB.addBirthDescription(String.raw`${AB.nickname} was born in Ur, in Mesopotamia. He is a legendary figure, not completely proven to be real.`);


    const JC = new Figure(
      "Jesus Christ",
      0,
      33,
      "https://en.wikipedia.org/wiki/Jesus_Christ",
      [Wisdom.CH],
      "Jesus"
    );
    JC.addBirthDescription(String.raw`${JC.nickname} was born in Bethlehem. Most scholars agree that he was a real historical figure, outside of the tales of his miracle work.`);

    const MS = new Figure(
      "Moses",
      -1391,
      -1271,
      "https://en.wikipedia.org/wiki/Moses",
      [Wisdom.JD, Wisdom.CH, Wisdom.IS]
    );
    MS.addBirthDescription(String.raw`${MS.nickname} was born in Goshen, in Lower Egypt. He is a legendary figure, not completely proven to be real.`);

    const BH = new Figure(
      "Gautama Buddha",
      -563,
      -483,
      "https://en.wikipedia.org/wiki/Gautama_Buddha",
      [Wisdom.BH],
      "Buddha"
    );
    BH.addBirthDescription(String.raw`${BH.nickname} was born in Lumbini, in modern day Nepal. Most scholars agree that he was a real historical figure, outside of the tales of his miracle work.`);

    const MO = new Figure(
      "Muhammad ibn Abdullah",
      570,
      632,
      "https://en.wikipedia.org/wiki/Muhammad",
      [Wisdom.IS],
      "Muhammad"
    );
    MO.addBirthDescription(String.raw`${MO.nickname} was born in Mecca. Most scholars agree that he was a real historical figure, outside of the tales of his miracle work.`);

    const LZ = new Figure(
      "Laozi",
      -369,
      -286,
      "https://en.wikipedia.org/wiki/Laozi",
      [Wisdom.TO]
    );
    LZ.addBirthDescription(String.raw`${LZ.nickname} was born in Chujen Village, modern day Luyi County in China. Most scholars agree that he was a real historical figure, outside of the tales of his miracle work.`);

    const CF = new Figure(
      "Confucius",
      -551,
      -479,
      "https://en.wikipedia.org/wiki/Confucius",
      [Wisdom.CF]
    );
    CF.addBirthDescription(String.raw`${CF.nickname} was born in modern day Shandong, China.`);

    const figures = [
      AB, JC, MS, BH, MO, LZ, CF
    ];

    const extraEvents = [
      {
        start_date: {year: -3500},
        text: {
          headline: "Earliest dating of the Kish Tablets",
          text: "Widely considered the oldest known form of writing, a proto-cuneiform."
        },
        media: {
          url: "https://en.wikipedia.org/wiki/Kish_tablet"
        }
      },
      {
        start_date: {year: -399},
        text: {
          headline: "Trial of Socrates"
        },
        media: {
          url: "https://en.wikipedia.org/wiki/Trial_of_Socrates"
        }
      },
      {
        start_date: {year: -375},
        text: {
          headline: 'Plato\'s "Republic"'
        },
        media: {
          url: "https://en.wikipedia.org/wiki/Republic_(Plato)"
        }
      },
      {
        start_date: {year: -900},
        text: {
          headline: "Oldest Olmec colossal head dated"
        },
        media: {
          url: "https://en.wikipedia.org/wiki/Olmec_colossal_heads"
        }
      },
      {
        start_date: {year: -1755},
        text: {
          headline: "Code of Hammurabi composed"
        },
        media: {
          url: "https://en.wikipedia.org/wiki/Code_of_Hammurabi"
        }
      },
      {
        start_date: {year: 325},
        text: {
          headline: "First Version of the Nicene Creed adopted"
        },
        media: {
          url: "https://en.wikipedia.org/wiki/Nicene_Creed"
        }
      },
      {
        start_date: {year: -44},
        text: {
          headline: "Julius Caesar is assassinated"
        },
        media: {
          url: "https://en.wikipedia.org/wiki/Ides_of_March"
        }
      },
      {
        start_date: {year: 220},
        text: {
          headline: "End of the Han Dynasty and start of the Three Kingdoms"
        },
        media: {
          url: "https://en.wikipedia.org/wiki/Three_Kingdoms"
        }
      },
      {
        start_date: {year: -850},
        text: {
          headline: "Homer's The Iliad and the Odyssey are written around this time"
        },
        media: {
          url: "https://en.wikipedia.org/wiki/Homer"
        }
      },
      {
        start_date: {year: -250},
        text: {
          headline: "Estimated date of composition for the Bhagavad Gita"
        },
        media: {
          url: "https://en.wikipedia.org/wiki/Bhagavad_Gita"
        }
      },
      {
        start_date: {year: -2100},
        text: {
          headline: "Oldest estimated dating of The Epic of Gilgamesh"
        },
        media: {
          url: "https://en.wikipedia.org/wiki/Epic_of_Gilgamesh"
        }
      }
    ];
    extraEvents.forEach(e => e.group = "World Events");

    return {
      events: figures.reduce((acc, f) => { return acc.concat(f.events); }, extraEvents),
      title: {
        text: {
          headline: "Leaders of Wisdom Traditions"
        }
      }
    };
  };
})();
